import { Component, ElementRef, ViewChild } from '@angular/core';
import { CatPictureService } from './services/catPicture.service';
import {HttpClient} from '@angular/common/http'



export interface Employee {
  id: number
  name: string
  surname: string
}

export interface EmployeeId {
  id: number
}

export interface EmployeeEdit {
  id: number
  name: string;
  surname: string;
}

export interface EmployeeEdit2 {
  id: number
  name: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild('form', {static: false}) inputRef: ElementRef

  id: -1;
  name: string;
  surname: string;

  title = 'my-project';
  users: any;
  isShow = false;
  eId: number;
  sName: string;
  sSurName: string;
  eName = '';
  eSurname = '';
  dId: number = -1;

  constructor(private catPictureService: CatPictureService, private http: HttpClient) { }
  ngOnInit() {
    this.GetData();
  }

  GetData() {
    this.catPictureService.get('https://localhost:44353/api/weatherForecast/gettest').subscribe(value => {
      this.users = value;
      console.log(value)
    })
  }

  addEmployee(e) {
    e.preventDefault();

    const newEmployee: Employee = {
      id: this.eId,
      name: this.eName,
      surname: this.eSurname
    }

    this.http.post<Employee>('https://localhost:44353/api/weatherForecast/posttest', newEmployee)
    .subscribe(user => {
      this.GetData();
    })
  }

  deletEmployee(event, idD) {
    event.preventDefault();

    const newEmployeeId: EmployeeId = {
      id: idD
    }
    // console.log(newEmployeeId);
    this.http.post<EmployeeId>('https://localhost:44353/api/weatherForecast/delete', newEmployeeId)
    .subscribe(resp => {
      this.GetData();
    })
  }

  seveEmployee(sId, sName) {
    const newEmployeeEdit:EmployeeEdit2 = {
      id: sId,
      name: sName,
    }
    console.log(newEmployeeEdit);
    // console.log(newEmployeeEdit);
    this.http.post<EmployeeEdit2>('https://localhost:44353/api/weatherForecast/update', newEmployeeEdit)
    .subscribe(resp => {
      this.GetData();
    })
  }
}
